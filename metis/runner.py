#  Copyright 2016-2017 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from datetime import datetime

from .output import print_decorated, safe_print


LOG_DECORATION = u'|> '


class Runner(object):

    def __init__(self, config, migration, database, table):
        self.config = config
        self.migration = migration
        self.database = database
        self.table = table

    def log(self, message, decorated=True, stdout=True):
        if not stdout:
            return
        if decorated:
            app_message = u'history: {}'.format(
                message,
            )
            print_decorated(app_message)
        else:
            safe_print(message)

    def perform(self):
        self.table.create_if_not_exists()

        for version in self.migration.versions:
            self.log(u'processing version {}'.format(version.number))
            VersionRunner(self, version).perform()


class VersionRunner(object):

    def __init__(self, runner, version):
        self.runner = runner
        self.table = runner.table
        self.migration = runner.migration
        self.config = runner.config
        self.database = runner.database
        self.version = version
        self.logs = []

    def log(self, message, decorated=True, stdout=True):
        self.logs.append(message)
        if not stdout:
            return
        if decorated:
            app_message = u'version {}: {}'.format(
                self.version.number,
                message,
            )
            print_decorated(app_message)
        else:
            safe_print(message)

    def perform(self):
        """Perform the version upgrade on the database.
        """
        db_versions = self.table.versions()

        version = self.version
        if (version.is_processed(db_versions)):
            self.log(
                u'version {} is already historized'.format(version.number)
            )
            return

        self.log(u'start')
        version_number = self.version.number
        self.table.add_version(version_number, datetime.now())
        for feature in self.version.features:
            self.table.add_task(version_number, feature, "feature")
        for bugfix in self.version.bugfixes:
            self.table.add_task(version_number, bugfix, "bugfix")
        self.log(u'finished')
        self.table.finish_version(version_number, datetime.now())
