#  Copyright 2016-2017 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import psycopg2

from collections import namedtuple
from contextlib import contextmanager


class Database(object):

    def __init__(self, config):
        self.config = config
        self.name = config.database

    def dsn(self):
        cfg = self.config
        params = {
            "dbname": cfg.database,
        }
        if cfg.db_host:
            params["host"] = cfg.db_host
        if cfg.db_port:
            params["port"] = cfg.db_port
        if cfg.db_user:
            params["user"] = cfg.db_user
        if cfg.db_password:
            params["password"] = cfg.db_password
        return params

    @contextmanager
    def connect(self, autocommit=False):
        with psycopg2.connect(**self.dsn()) as conn:
            if autocommit:
                conn.autocommit = True
            yield conn

    @contextmanager
    def cursor_autocommit(self):
        with self.connect(autocommit=True) as conn:
            with conn.cursor() as cursor:
                yield cursor


VersionRecord = namedtuple(
    'VersionRecord',
    'number date_start date_done'
)


class HistoryTable(object):

    def __init__(self, database):
        self.database = database
        self.table_name = "metis_version"
        self.task_table_name = "metis_feature"
        self.bugfix_table_name = "metis_bugfix"
        self.VersionRecord = VersionRecord
        self._versions = None


    def create_if_not_exists(self):
        with self.database.cursor_autocommit() as cursor:
            query = """
            CREATE TABLE IF NOT EXISTS {metis_table} (
                number VARCHAR NOT NULL,
                date_start TIMESTAMP NOT NULL,
                date_done TIMESTAMP,
                CONSTRAINT metis_version_pk PRIMARY KEY (number)
            );
            
            CREATE TABLE IF NOT EXISTS {task_table} (
                id SERIAL PRIMARY KEY,
                type VARCHAR NOT NULL,
                code VARCHAR NOT NULL,
                summary TEXT,
                detail TEXT,
                version_number VARCHAR,
                CONSTRAINT fk_metis_version FOREIGN KEY(version_number) REFERENCES {metis_table}(number)
            );
            """.format(
                metis_table=self.table_name, task_table=self.task_table_name)
            cursor.execute(query)

    def versions(self):
        """ Read versions from the table
        The versions are kept in cache for the next reads.
        """
        if self._versions is None:
            with self.database.cursor_autocommit() as cursor:
                query = """
                SELECT number,
                       date_start,
                       date_done
                FROM {}
                """.format(self.table_name)
                cursor.execute(query)
                rows = cursor.fetchall()
                versions = []
                for row in rows:
                    row = list(row)
                    versions.append(
                        self.VersionRecord(*row)
                    )
                self._versions = versions
        return self._versions

    def add_version(self, number, start):
        with self.database.cursor_autocommit() as cursor:
            query = """
            SELECT number FROM {}
            WHERE number = %s
            """.format(self.table_name)
            cursor.execute(query, (number,))
            if cursor.fetchone():
                query = """
                UPDATE {}
                SET date_start = %s,
                    date_done = NULL
                WHERE number = %s
                """.format(self.table_name)
                cursor.execute(query, (start, number))
            else:
                query = """
                INSERT INTO {}
                (number, date_start)
                VALUES (%s, %s)
                """.format(self.table_name)
                cursor.execute(query, (number, start))
        self._versions = None  # reset versions cache

    def add_task(self, number, task, task_type):
        with self.database.cursor_autocommit() as cursor:
            query = """
            INSERT INTO {}
            (code, summary, detail, type, version_number)
            VALUES (%s, %s, %s, %s, %s)
            """.format(self.task_table_name)
            cursor.execute(query, (task["code"], task["summary"],
                                   task["detail"], task_type, number))

    def finish_version(self, number, end):
        with self.database.cursor_autocommit() as cursor:
            query = """
            UPDATE {}
            SET date_done = %s
            WHERE number = %s
            """.format(self.table_name)
            cursor.execute(query, (end, number))
            self._versions = None  # reset versions cache
