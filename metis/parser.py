#  Copyright 2016-2018 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import io
import warnings

from ruamel.yaml import YAML
from .exception import ParseError
from .version import FIRST_VERSION
from .model import Version, History


class YamlParser(object):
    def __init__(self, parsed):
        self.parsed = parsed

    @classmethod
    def parser_from_buffer(cls, fp):
        """Construct YamlParser from a file pointer."""
        yaml = YAML(typ="safe")
        return cls(yaml.load(fp))

    @classmethod
    def parse_from_file(cls, filename):
        """Construct YamlParser from a filename."""
        with io.open(filename, 'r', encoding='utf-8') as fh:
            return cls.parser_from_buffer(fh)

    def check_dict_expected_keys(self, expected_keys, current, dict_name):
        """ Check that we don't have unknown keys in a dictionary.
        It does not raise an error if we have less keys than expected.
        """
        if not isinstance(current, dict):
            raise ParseError(u"'{}' key must be a dict".format(dict_name),
                             "YAML_EXAMPLE")
        expected_keys = set(expected_keys)
        current_keys = {key for key in current}
        extra_keys = current_keys - expected_keys
        if extra_keys:
            message = u"{}: the keys {} are unexpected. (allowed keys: {})"
            raise ParseError(
                message.format(
                    dict_name,
                    list(extra_keys),
                    list(expected_keys),
                ),
                "YAML_EXAMPLE",
            )

    def parse(self):
        """Check input and return a :class:`Migration` instance."""
        if not self.parsed.get("history"):
            raise ParseError(u"'history' key is missing", "YAML_EXAMPLE")
        self.check_dict_expected_keys(
            {'versions'}, self.parsed['history'], 'history',
        )
        return self._parse_history()

    def _parse_history(self):
        history = self.parsed["history"]
        versions = self._parse_versions(history)
        return History(versions)

    def _parse_versions(self, history):
        versions = history.get("versions") or []
        if not isinstance(versions, list):
            raise ParseError(u"'versions' key must be a list", "YAML_EXAMPLE")
        if versions[0]['version'] != FIRST_VERSION:
            warnings_msg = u'First version should be named `setup`'
            warnings.warn(warnings_msg, FutureWarning)
        return [self._parse_version(version) for version in versions]

    def _parse_version(self, parsed_version):
        self.check_dict_expected_keys(
            {"version", "features", "bugfixes"},
            parsed_version, "versions",
        )
        number = parsed_version.get("version")
        version = Version(number)

        features = parsed_version.get("features") or {}
        self._parse_features(version, features)

        bugfixes = parsed_version.get("bugfixes") or {}
        self._parse_bugfixes(version, bugfixes)
        return version

    def _parse_features(self, version, features):
        for feature in features:
            vals = self._parse_task(feature)
            version.add_feature(vals)

    def _parse_bugfixes(self, version, bugfixes):
        for bugfix in bugfixes:
            vals = self._parse_task(bugfix)
            version.add_bugfix(vals)

    def _parse_task(self, task):
        return {
            "code": task.get("code", ""),
            "summary": task.get("summary", ""),
            "detail": task.get("detail", ""),
        }
