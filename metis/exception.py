#  Copyright 2016-2018 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

class MetisError(Exception):
    pass


class ParseError(MetisError):

    def __init__(self, message, example=None):
        super(ParseError, self).__init__(message)
        self.example = example

    def __str__(self):
        if not self.example:
            return super(ParseError, self).__str__()
        msg = (u'An error occured during the parsing of the configuration '
               u'file. Here is an example to help you to figure out '
               u'your issue.\n{}\n{}').format(self.example, self.args[0])
        return msg


class ConfigurationError(MetisError):
    pass


class OperationError(MetisError):
    pass
