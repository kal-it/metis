#  Copyright 2016-2018 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from .version import HistoryVersion
from .exception import ConfigurationError


class History(object):

    def __init__(self, versions):
        self._versions = versions

    @property
    def versions(self):
        return sorted(self._versions, key=lambda v: HistoryVersion(v.number))


class Version(object):

    def __init__(self, number):
        """
        Base class for a history version
        :param number: Valid version number
        :type number: String
        """

        try:
            HistoryVersion().parse(number)
        except ValueError:
            raise ConfigurationError(
                u'{} is not a valid version'.format(number)
            )
        self.number = number
        self.features = []
        self.bugfixes = []

    def is_processed(self, db_versions):
        """Check if version is already applied in the database.
        :param db_versions:
        """
        return self.number in (v.number for v in db_versions if v.date_done)

    def add_feature(self, feature):
        self.features.append(feature)

    def add_bugfix(self, bugfix):
        self.bugfixes.append(bugfix)


class Task(object):

    def __init__(self, code, summary, detail):
        self.code = code
        self.summary = summary
        self.detail = detail


class Feature(Task):
    pass


class Bugfix(Task):
    pass
