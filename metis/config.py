#  Copyright 2016-2018 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from distutils.util import strtobool

import argparse
import os


class Config(object):

    def __init__(self, history_file, database, db_user=None, db_password=None,
                 db_port=5432, db_host="localhost"):
        self.history_file = history_file
        self.database = database
        self.db_user = db_user
        self.db_password = db_password
        self.db_port = db_port
        self.db_host = db_host

    @classmethod
    def from_parse_args(cls, args):
        """Constructor from command line args.
        :param args: parse command line arguments
        :type args: argparse.ArgumentParser
        """

        return cls(args.history_file, args.database, db_user=args.db_user,
                   db_password=args.db_password, db_port=args.db_port,
                   db_host=args.db_host,)


class EnvDefault(argparse.Action):

    def __init__(self, envvar, required=True, default=None, **kwargs):
        if not default and envvar:
            default = self.get_default(envvar)
        if required and default is not None:
            required = False
        super(EnvDefault, self).__init__(default=default, required=required,
                                         **kwargs)

    def get_default(self, envvar):
        return os.getenv(envvar)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)


class BoolEnvDefault(EnvDefault):

    def get_default(self, envvar):
        val = os.getenv(envvar, '')
        try:
            return strtobool(val.lower())
        except ValueError:
            return False


def get_args_parser():
    """Return a parser for command line options."""
    parser = argparse.ArgumentParser(
        description="Metis: history for Odoo")
    parser.add_argument("--history-file", "-f",
                        action=EnvDefault,
                        envvar="METIS_HISTORY_FILE",
                        required=True,
                        help="The yaml file containing the history")

    parser.add_argument('--database', '-d',
                        action=EnvDefault,
                        envvar='METIS_DATABASE',
                        required=True,
                        help="Odoo's database")
    parser.add_argument('--db-user', '-u',
                        action=EnvDefault,
                        envvar='METIS_DB_USER',
                        required=True,
                        help="Odoo's database user")
    parser.add_argument('--db-password', '-w',
                        action=EnvDefault,
                        envvar='METIS_DB_PASSWORD',
                        required=True,
                        help="Odoo's database password")
    parser.add_argument('--db-port', '-p',
                        type=int,
                        default=os.environ.get('METIS_DB_PORT', 5432),
                        help="Odoo's database port")
    parser.add_argument('--db-host', '-H',
                        default=os.environ.get('METIS_DB_HOST',
                                               'localhost'),
                        help="Odoo's database host")
    return parser
