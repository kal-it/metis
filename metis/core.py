#  Copyright 2016-2018 Camptocamp SA
#  Copyright 2022 Kal-IT
#  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from .config import Config, get_args_parser
from .database import Database, HistoryTable
from .parser import YamlParser
from .output import safe_print
from .runner import Runner

import time
import threading


ADVISORY_LOCK_IDENT = 7141416871201361999


def pg_advisory_lock(cursor, lock_ident):
    cursor.execute('SELECT pg_try_advisory_xact_lock(%s);', (lock_ident,))
    acquired = cursor.fetchone()[0]
    return acquired


class ApplicationLock(threading.Thread):

    def __init__(self, connection):
        self.acquired = False
        self.connection = connection
        self.replica = False
        self.stop = False
        super(ApplicationLock, self).__init__()

    def run(self):
        with self.connection.cursor() as cursor:
            # If the migration is run concurrently (in several
            # containers, hosts, ...), only 1 is allowed to proceed
            # with the migration. It will be the first one to win
            # the advisory lock. The others will be flagged as 'replica'.
            while not pg_advisory_lock(cursor, ADVISORY_LOCK_IDENT):
                if not self.replica:  # print only the first time
                    safe_print('A concurrent process is already '
                               'running the migration')
                self.replica = True
                time.sleep(0.5)
            else:
                self.acquired = True
                idx = 0
                while not self.stop:
                    # keep the connection alive to maintain the advisory
                    # lock by running a query every 30 seconds
                    if idx == 60:
                        cursor.execute("SELECT 1")
                        idx = 0
                    idx += 1
                    # keep the sleep small to be able to exit quickly
                    # when 'stop' is set to True
                    time.sleep(0.5)


def historize(config):
    """Perform a historization according to config
        :param config: The configuration to be applied
        :type config: Config
    """
    history_parser = YamlParser.parse_from_file(config.history_file)
    history = history_parser.parse()

    database = Database(config)

    with database.connect() as lock_connection:
        application_lock = ApplicationLock(lock_connection)
        application_lock.start()

        while not application_lock.acquired:
            time.sleep(0.5)
        else:
            if application_lock.replica:
                # when a replica could finally acquire a lock, it
                # means that the concurrent process has finished the
                # migration or that it failed to run it.
                # In both cases after the lock is released, this process will
                # verify if it has still to do something (if the other process
                # failed mainly).
                application_lock.stop = True
                application_lock.join()
            # we are not in the replica or the lock is released: go on for the
            # migration

        try:
            table = HistoryTable(database)
            runner = Runner(config, history, database, table)
            runner.perform()
        finally:
            application_lock.stop = True
            application_lock.join()


def main():
    """Parse the command line and run :func:`migrate`."""
    parser = get_args_parser()
    args = parser.parse_args()
    config = Config.from_parse_args(args)
    historize(config)


if __name__ == '__main__':
    main()
