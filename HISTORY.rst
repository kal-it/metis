.. :changelog:

Release History
---------------

0.1.0 (2022-11-17)
++++++++++++++++++

Initial release. This corresponds to the initial work of Timothée Ringeard.
