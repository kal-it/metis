# -*- coding: utf-8 -*-
import sys

from setuptools import setup, find_packages

with open("HISTORY.rst") as f:
    history = f.read()

test_deps = [
    "pytest",
    "mock",
]

extras = {
    "test": test_deps,
}

setup(
    name="metis",
    use_scm_version=True,
    description="History tool for Odoo",
    long_description=history,
    author="Kal-IT (Timothée Ringeard)",
    author_email="timothee.ringeard@kal-it.fr",
    url="",
    license="AGPLv3+",
    packages=find_packages(exclude=("tests", "docs")),
    install_requires=[
        "psycopg2-binary",
        "ruamel.yaml>=0.15.1",
        "future",
    ],
    setup_requires=[
        "setuptools_scm",
    ],
    tests_require=test_deps,
    extras_require=extras,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: "
        "GNU Affero General Public License v3 or later (AGPLv3+)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
    entry_points={
        "console_scripts": ["metis = metis.core:main"]
    },
)
